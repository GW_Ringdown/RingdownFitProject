# The project
 
The GW_Rdown project aims at producing accurate ringdown (RD) models that describe the final state of binary black hole mergers. <br>
The RD emerges as the late trail of radiation usually represented as a sum of damped sinusoids and its modelling is essential to test general relativity (GR) in its most extreme regime.  


# Prerequisites

To fully and succesufully use all codes stored here, one needs to have the following codes and versions installed:
* Python ≥ 3.0 along with the qnm package [2-3] and the numpy package;
* Mathematica ≥ 11.0;


# Structure

In this repository one can find: 
* codes to generate the models;
* data describing the quasi-normal mode (QNM) spectrum for some modes, used to generate the models that contain these modes;
* data resulting from fits informed by numerical relativity (NR) waveforms;

all from reference [1].<br>

**Please refer to the above paper if using these codes and results.**


Structure:
* ./codes/Mathematica:\
It contains Rdown.nb and Rdown.m. These two (equivalent) codes contain the main functions                                      used to produce the RD models and ansätze (OvertoneModel), to compute the QNM frequencies and damping times (\\[Omega]lmnPy) from                                    the python code qnm [2-3] complemented by our tables for the (l=2,m=2,n=8,9) modes ([1]), or to estimate the final mass and spin
                               from the GW strain (FitRingdownGrid).
                             
* ./data :    
  * /QNMdata. It contains the QNM data (tables) for the lmn = 228 and lmn = 229 co-rotating and lmn = 22(8-9) counter-rotating modes, as decribed in reference [1].
  * /NRFits. NR fit results obtained by reference [1].                                  

# The data/NRFits folder.

You will find there a list of BBH_SXS_index folders, each one corresponding to one of the 620 SXS NR ringdown fits produced in [1]. In each folder you will find three files:

* mass_spin.dat: It contains the normalised final mass and the final dimensionless spin of the given simulation, read off from the simulation's metadata.
* datafile_nmin0_nmax12.dat: It contains the results of the fits in the following format: mass, spin , mismatch, epsilon, Bayesian Information Criterion (BIC).
* datafile_BFAmplitudes_nmin0_nmax12.mx. It contains the results of the best fit amplitudes for each NR case. You need Mathematica to load the .mx files.

  
    
# References

* [1] F. Jiménez Forteza and P. Mourier, "High-overtone fits to numerical relativity ringdowns: beyond the dismissed n=8 special tone".  (2021).  [arXiv: 2107.11829]

* [2] L. C. Stein, "qnm: A Python package for calculating Kerr quasinormal modes, separation constants, and spherical-spheroidal mixing coefficients",  J. Open Source Softw. **4**:1683 (2019). [arXiv:1908.10377]

* [3] https://pypi.org/project/qnm/
